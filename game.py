# Import des classes ball et paddle 
import pygame

pygame.init()
 
# Définir les deux couleurs 
NUMBER_1 = "1"
WHITE = (255,255,255)
NUMBER_2 = "2"
RED =(255,0,0)
NUMBER_3 = "3"
ORANGE = (255,127,7)
NUMBER_4 = "4"
YELLOW = (255, 255, 7)
NUMBER_5 = "5" 
GREEN = (52, 102, 97)
NUMBER_6 = "6"
INDIGO = (17,198,174)
NUMBER_7 = "7"
BLUE = (17, 26, 198)
NUMBER_8 = "8"
VIOLETTE = (148,21,239)
NUMBER_9 = "9"
BLACK = (0,0,0)

BLUEDUCK = (52, 102, 97)
GREY = (211, 211, 211)
RIGHT = "Caneva RIL19"
TEXT_LEFT = "Saisir un numéro de couleur correspondant"

# Déploie une fenêtre
size = (900, 500)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("CANEVA UNIVERSEL RIL19 Multijoueurs")

# Contrôle de nb d'image à la seconde / Eviter les sentillements
clock = pygame.time.Clock()

running = True

while running:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
              running = False
        elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE: 
                     running=False

    screen.fill(GREY)
    
    # Emplacement Pallette
    pygame.draw.rect(screen, WHITE, (10,120,30,30))
    pygame.draw.rect(screen, RED, (40,120,30,30))
    pygame.draw.rect(screen, ORANGE, (70,120,30,30))
    pygame.draw.rect(screen, YELLOW, (100,120,30,30))
    pygame.draw.rect(screen, GREEN, (130,120,30,30))
    pygame.draw.rect(screen, INDIGO, (160,120,30,30))
    pygame.draw.rect(screen, BLUE, (190,120,30,30))
    pygame.draw.rect(screen, VIOLETTE, (220,120,30,30))
    pygame.draw.rect(screen, BLACK, (250,120,30,30))

    # Titre du caneva à dessiner
    font1 = pygame.font.Font(None, 30)
    font2 = pygame.font.Font(None, 25)

    text = font1.render(str(RIGHT), 1, BLUEDUCK)
    screen.blit(text, (600,10))
    text = font2.render(str(TEXT_LEFT),1 , BLUEDUCK)
    screen.blit(text,(10,80))

    # Numérotation des couleurs
    text = font2.render(str(NUMBER_1),1, BLACK)
    screen.blit(text,(20,125))
    text = font2.render(str(NUMBER_2),1 , BLACK)
    screen.blit(text,(50, 125))
    text = font2.render(str(NUMBER_3),1 , BLACK)
    screen.blit(text,(80, 125))
    text = font2.render(str(NUMBER_4),1 , BLACK)
    screen.blit(text,(110, 125))
    text = font2.render(str(NUMBER_5),1 , BLACK)
    screen.blit(text,(140, 125))
    text = font2.render(str(NUMBER_6),1 , BLACK)
    screen.blit(text,(170, 125))
    text = font2.render(str(NUMBER_7),1 , BLACK)
    screen.blit(text,(200, 125))
    text = font2.render(str(NUMBER_8),1 , BLACK)
    screen.blit(text,(230, 125))
    text = font2.render(str(NUMBER_9),1 , WHITE)
    screen.blit(text,(260, 125))

    pygame.display.flip()

    clock.tick(60)

pygame.quit()
